# Plano da Disciplina - Aprendizado de Máquina (FGA0083)

## Professor
* Fabricio Ataides Braz

## Período
2º Semestre de 2.023

## Turma
1

## Ementa
* Introdução a métodos de aprendizado de máquina que são comumente utilizados em aplicações de reconhecimento de padrões em sinais (texto, som e imagem). 
* Regressão. 
* Classificação 
* Aprendizado não supervisionado. 
* Máquinas de vetores de suporte. 
* Redes neurais artificiais. 

## Método

Independente do método de ensino, a construção de modelos de Inteligencia Artificial envolve conhecimentos, cuja apreensão demanda experimentos contínuos de exercício das suas técnicas e fundamentos. Várias abordagens servem ao própósito de motivar o aluno a buscar esse conhecimento. 

O ensino neste semestre será orientado pela revisão de literatura e do material instrucional do curso Fastai 2022, além de experimentos científicos de modelagem de aprendizado de máquina, sucedido de relato na forma de artigo para blog.

Essa proposta, se inspira muito no aprendizado baseado em projeto, visto que cada relato demanda a pesquisa e desenvolvimento de experimentos diversos. Isso faz com que o foco saia da **instrução**, em que o professor em sala de aula instrui o aluno sobre conceitos e ferramentas, para a **investigação**, em que o aluno é desafiado a pesquisar conceitos, técnicas e ferramentas para conseguir alcançar os objetivos do artigo desejado. A perspectiva do professor muda da **instrução**, para a **tutoria**, no que diz respeito ao ensino. A perspectiva do aluno muda de **passiva** para **ativa**, no que diz respeito ao aprendizado.

A disciplina prevê um total de 60 horas de formação.

No que diz repeito a abordagem técnica para aprendizado de máquina, daremos preferência aos **modelos de aprendizagem profunda** (*deep learning*). Em razao disso, o tópico **redes neurais artificiais** será base para o ensino de modelagem supervisionada (classifição/regressão). Além disso, ao invés de modelos de maquina de suporte, o conteúdo da disciplina traz árvores de decisão.

## Ferramentas & Materiais
* Teams - Comunicação e trabalho colaborativo - código da equipe r944wqa
* Python - Linguagem de programação;
* [Forum de Discussão](https://forum.ailab.unb.br)
* [Gitlab](https://github.com/fabraz/fastaiOnCampus)

## Avaliação

Para que o aluno seja aprovado na disciplina ele deve possuir média final superior ou igual a 50, correspondente a menção MM. Além disso, seu comparecimento deve ser superior ou igual a 75% dos eventos estabelecidos pela disciplina. 

### Composição da Média Final

Os alunos serão avaliados individualmente, tendo como objeto de avaliação um artigo para blog em que algum [conceito](https://course.fast.ai/Lessons/Summaries/) da lição do [fastai](https://course.fast.ai/) na respectiva semana seja abordado. Como são 8(oito) lições, ao longo do semestre, serão 8(oito) artigos avaliados. A equação a seguir detalha como as notas individuais comporão a média final. Chamo a atenção por se tratar de média ponderada, em que o peso vai crescendo, à medida que o semestre avança.

![equation](https://latex.codecogs.com/svg.image?media_final&space;=&space;\frac{\sum_{l=1}^8&space;l*n_l}{\sum_{l=1}^8&space;l&space;})

Onde `l` é o número da lição e `n` a respectiva nota. 

Especificamente, cada artigo será avaliado segundo os seguintes critérios:

|#|Crietério|Detalhe|Eliminatório|Percentual|
|---|---|---|---|---|
|1|Propósito|O artigo precisa ter um propósito, pois ele é quem pauta todo<br/>o conteúdo|Sim|10%|
|2|Tema e dado abordado coerente com a lição| O tema abordado pelo artigo foi contemplado na lição do fastai <br/>O dado usado para modelagem precisa ser coerente <br/>com a tarefa abordada na lição|Sim|-|
|3|Caracterização do dado| O dado foi devidamente caracterizado, como <br/>detalhes sobre o fluxo que ele percorre, desde seu,<br/> armazenamento, até o seu processamento em modelagem|Não|25%|
|4|Organização| O artigo possui estrutura que favoreça a sua compreensão|Não|10%|
|5|jupyter| O artigo foi elaborado usando o jupyter| Sim| -|
|6|Modelagem| A seção de modelagem está devidamente caracterizada e explicada|Sim|25%|
|7|Inferência| A seção de inferência está devidamente caracterizada e explicada.<br/>A inferência conta com app no huggingface (a partir da lição 2)|Sim|15%|
|8|Nova base| A base de dados é diferente da apresentada na lição e diferente de<br/>artigos anteriores|Sim|-|
|9|Desenvolvimento| O conteúdo relativo ao desenvolvimentoda ideia proposta pelo<br/>artigo conta com código,imagens, graficos para facilitar a <br/>compreensão da questão|Não|15%|
|10|Plágio|O conteúdo do artigo precisa ter sido elaborado pelo próprio aluno, a exceção de citações|Sim|-|
|11|Prazo|Pull request enviado até as 11:59 do dia anterior a avaliação|Sim|-|
|12|Domínio| O aluno precisa ter domínio do conteúdo divulgado no artigo|Sim|-|


> Caso o critério eliminatório não puder ser reconhecido no artigo, a nota do artigo será anulada.

Serão sorteados artigos para serem apresentados conforme data no calendário a seguir, de acordo com a lição, momento em que o professor arguirá o aluno sobre o seu conteúdo. Caso o aluno não demonstre domínio sobre o conteúdo, a nota da atividade será anulada.

1. Getting started
2. Deployment
3. Neural net foundations
4. Natural Language (NLP)
5. From-scratch model
6. Random forests
7. Collaborative filtering
8. Convolutions (CNNs)

| Aula | Data       | Assunto    |
|------|------------|------------|
| 1    | 29-08-2023 | Acolhida   |
| 2    | 31-08-2023 | lesson 0   |
| 3    | 05-09-2023 | lesson 1   |
| -    | 07-09-2023 | Feriado Independência do Brasil   |
| 4    | 12-09-2023 | **lesson 1 - avaliação**|
| 5    | 14-09-2023 | lesson 2   |
| 6    | 19-09-2023 | lesson 2   |
| 7    | 21-09-2023 | **lesson 2 - avaliação**   |
| 8    | 26-09-2023 | **Semana acadêmica** |
| 9    | 28-09-2023 | **Semana acadêmica**|
| 10   | 03-10-2023 | lesson 3   |
| 11   | 05-10-2023 | lesson 3   |
| 12   | 10-10-2023 | **lesson 3 - avaliação**|
| -   | 12-10-2023 | Padroeira do Brasil   |
| 13   | 17-10-2023 | lesson 4   |
| 14   | 19-10-2023 | lesson 4   |
| 15   | 24-10-2023 | lesson 4   |
| 16   | 26-10-2023 | **lesson 4 - avaliação**|
| 17   | 31-10-2023 | lesson 5   |
| -   | 02-11-2023 | Finados |
| 18   | 07-11-2023 | lesson 5   |
| 19   | 09-11-2023 | **lesson 5 - avaliação**|
| 20   | 14-11-2023 | lesson 6 |
| 21   | 16-11-2023 | lesson 6   |
| 22  | 18-11-2023 | **Sabado - 10h-11:50** <br/>lesson 6|
| 22   | 21-11-2023 | **lesson 6 - avaliação**   |
| 23   | 23-11-2023 |  lesson 7|
| 24   | 28-11-2023 | lesson 7 |
| 25   | 30-11-2023 | lesson 7   |
| 26   | 02-12-2023 | **Sabado - 8h-9:50** <br/>lesson 7|
| 27   | 05-12-2023 |**lesson 7 - avaliação**   |
| 28   | 07-12-2023 | lesson 8    |
| -   | 12-12-2023 | **Conferencia**|
| -   | 14-12-2023 | **Conferencia**   |
| 29   | 19-12-2023 | lesson 8  |
| 30   | 21-12-2023 | **lesson 8 - avaliação**  |

## Referências Bibliográficas

### Básica

* Kevin Patrick Murphy. Machine Learning: a Probabilistic Perspective Editor MIT press. 2012. Cambridge, MA.

* Chris Bishop. Pattern Recognition and Machine Learning. Editor Springer. 2006. New York.

* Ian Goodfellow, Yoshua Bengio, Aaron Courville. Deep Learning Editor MIT press. 2017. Cambridge, MA.

* [Yaser S. Abu-Mostafa, Malik Magdon-Ismail, Hsuan-Tien Lin. Learning from Data - a Short Course.](https://work.caltech.edu/telecourse.html). AML Book. 2012. Pasadena, CA. 

* Tom M. Mitchell. Machine Learning Editor. McGraw-Hill. 1997 

* David Barber. Bayesian Reasoning and Machine Learning. Cambridge University Press. 2012. Cambridge, UK. 

* Carl Edward Rasmussen, Christopher K. I. Williams. Gaussian Processes For Machine Learning Editor MIT press. 2006. Cambridge, MA 

* [Andrew Ng Local. Machine Learning Video Lectures](https://www.coursera.org/learn/machine-learning). Stanford, CA. 2014

### Complementar

* [Deep Learning with Pytorch](https://pytorch.org/assets/deep-learning/Deep-Learning-with-PyTorch.pdf)
* [Jeremy Howard and Sylvain Gugge. FastBook](https://github.com/fastai/fastbook)
* [AI Lab Forum](https://forum.ailab.unb.br)


